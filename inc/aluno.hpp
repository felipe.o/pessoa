#ifndef ALUNO_H
#define ALUNO_H

#include <string>
#include "pessoa.hpp"


using namespace std;

class Aluno: public Pessoa {
private:
	string matricula;
	int qtd_creditos;
	string curso;
public:
	Aluno();
	Aluno(string nome, string idade, string telefone, string matricula, int qtd_creditos, string curso);
	void setMatricula(string matricula);
	void setQtdCreditos(int qtd_creditos);
	void setCurso(string curso);
	string getMatricula();
	int getQtdCreditos();
	string getCurso();
};


#endif
