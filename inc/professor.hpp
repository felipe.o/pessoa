#ifndef PROFESSOR_H
#define PROFESSOR_H

#include "pessoa.hpp"
#include "string"

using namespace std;



class Professor: public Pessoa{
private:
	int hrs_semanais;
	int codigo_materia;
	string nome_materia;
public:
	Professor();
	Professor(string nome, string idade, string telefone, int hrs_semanais, int codigo_materia, string nome_materia);
	void setHrsSemanais(int hrs_semanais);
	void setCodigoMateria(int codigo_materia);
	void setNomeMateria(string nome_materia);
	int getHrsSemanais();
	int getCodigoMateria();
	string getNomeMateria();
};

#endif
