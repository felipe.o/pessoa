#include "professor.hpp"
#include <string>

using namespace std;

Professor::Professor(){
	setNome("..."); 
        setIdade("...");
        setTelefone("xxxx-xxxx");
        setHrsSemanais(0);
        setCodigoMateria(0);
        setNomeMateria("...");
}

Professor::Professor(string nome, string idade, string telefone, int hrs_semanais, int codigo_materia, string nome_materia){
	setNome(nome);
	setIdade(idade);
	setTelefone(telefone);
	setHrsSemanais(hrs_semanais);
	setCodigoMateria(codigo_materia);
	setNomeMateria(nome_materia);
}

void Professor::setHrsSemanais(int hrs_semanais){
	this->hrs_semanais=hrs_semanais;
}

void Professor::setCodigoMateria(int codigo_materia){
	this->codigo_materia=codigo_materia;
}

void Professor::setNomeMateria(string nome_materia){
	this->nome_materia=nome_materia;
}

int Professor::getHrsSemanais(){
	return hrs_semanais;
}

int Professor::getCodigoMateria(){
	return codigo_materia;
}

string Professor::getNomeMateria(){
	return nome_materia;
}
