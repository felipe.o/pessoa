#include <iostream>
#include "aluno.hpp"
#include "professor.hpp"

using namespace std;

int main(int argc, char ** argv) {
	Aluno *aluno_1;
	Professor *professor_1;

	aluno_1 = new Aluno("Felipe", "18", "6133333333", "140138676", 60, "Software");

	professor_1 = new Professor();

	cout << "Nome: " << aluno_1->getNome() << endl << "Idade: " << aluno_1->getIdade() << endl << 
	"Telefone: "<< aluno_1->getTelefone() << endl << "Matricula: " << aluno_1->getMatricula() << 
	endl <<"Quantidade de creditos: " << aluno_1->getQtdCreditos() << endl << "Curso: "<< aluno_1->getCurso()
	<< endl;

	professor_1->setNome("Tiago"); professor_1->setIdade("35"); professor_1->setTelefone("6134444444");
	professor_1->setHrsSemanais(40);professor_1->setCodigoMateria(130000); professor_1->setNomeMateria("OO");

	cout << endl;

	cout << "Nome: " << professor_1->getNome() << endl << "Idade: " << professor_1->getIdade() << endl << 
	"Telefone: "<< professor_1->getTelefone() << endl << "Horas semanais: "<<professor_1->getHrsSemanais()<< 
	endl <<"Codigo da materia: " << professor_1->getCodigoMateria() << endl << "Nome da materia: "<< 
	professor_1->getNomeMateria()	<< endl;

	delete(aluno_1);
	delete(professor_1);
}
