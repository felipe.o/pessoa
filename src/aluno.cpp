#include "aluno.hpp"
#include <iostream>
#include <string>

using namespace std;

Aluno::Aluno(){
	nome="...";
	idade="...";
	telefone="xxxx-xxxx";
	matricula="xxxxxxxxx";
	qtd_creditos=0;
	curso="...";
}

Aluno::Aluno(string nome, string idade, string telefone, string matricula, int qtd_creditos, string curso){
	this->nome=nome;
	this->idade=idade;
	this->telefone=telefone;
	this->matricula=matricula;
	this->qtd_creditos=qtd_creditos;
	this->curso=curso;
}

void Aluno::setMatricula(string matricula){
	this->matricula=matricula;
}

void Aluno::setQtdCreditos(int qtd_creditos){
	this->qtd_creditos=qtd_creditos;
}

void Aluno::setCurso(string curso){
	this->curso=curso;
}

string Aluno::getMatricula(){
	return matricula;
}

int Aluno::getQtdCreditos(){
	return qtd_creditos;
}

string Aluno::getCurso(){
	return curso;
}
